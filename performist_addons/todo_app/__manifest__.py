# -*- encoding: utf-8 -*-

{
    'name': 'To-Do',
    'summary': 'maîtriser, Comprendre le Framework',
    'decription': 'Gerer vos Tache Personnel avec ce module',
    'depends': ['mail'],
    'category': 'Apprentissage',
    'website': 'https://www.odoo.com',
    'author': 'Mahamat Abakar Mahamat',
    'application': True,
    'license': 'LGPL-3',
    'data': [

        
        'views/todo_view.xml',
        
        


    ],
    'demo': [
        
    ],
}